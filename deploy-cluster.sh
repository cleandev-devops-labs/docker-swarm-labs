#!/bin/bash

########### CREAMOS LOS NODOS DEL CLUSTER EN FORMA DE CONTENEDORES
docker run --privileged -h manager-01 --name manager-01 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind

docker run --privileged -h manager-02 --name manager-02 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind

docker run --privileged -h manager-03 --name manager-03 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind


docker run --privileged -h worker-01 --name worker-01 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind

docker run --privileged -h worker-02 --name worker-02 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind


docker run --privileged -h worker-03 --name worker-03 -d \
    --network docker-network-cluster \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v docker-cluster-certs-ca:/certs/ca \
    -v docker-cluster-certs-client:/certs/client \
    -v $(pwd)/share-folder:/root/share-folder \
    docker:dind


########### INSTALAMOS LA TERMINAL BASH EN TODOS LOS NODOS

docker exec manager-01 apk add bash
docker exec manager-02 apk add bash
docker exec manager-03 apk add bash
docker exec worker-01 apk add bash
docker exec worker-02 apk add bash
docker exec worker-03 apk add bash

sleep 5

########### PREPARAMOS LOS NODOS PARA FORMAR EL CLUSTER DE DOCKER SWARM
docker exec manager-01 docker swarm init --advertise-addr 'eth0'
join_token_worker=$(docker exec manager-01 docker swarm join-token worker | grep "docker swarm")
join_token_manager=$(docker exec manager-01 docker swarm join-token manager | grep "docker swarm")

########### UNIMOS LOS NODOS
docker exec manager-02 $join_token_manager
docker exec manager-03 $join_token_manager
docker exec worker-01 $join_token_worker
docker exec worker-02 $join_token_worker
docker exec worker-03 $join_token_worker


########### UTILIDADES
# Cambia el prompt de la maquina
# -> export PS1='\u@\h \W]\$'